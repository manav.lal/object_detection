import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:object_detection/main.dart';
import 'package:object_detection/view.dart';
import 'package:tflite/tflite.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  bool isWorking = false;
  String result = "";
  XFile? picture;
  CameraController? cameraController;
  CameraImage? cameraImage;
  loadModel() {
    Tflite.loadModel(
        model: "assets/mobilenet_v1_1.0_224.tflite",
        labels: "assets/labels.txt");
  }

  Future takePicture() async {
    if (!cameraController!.value.isInitialized) {
      return null;
    }
    if (cameraController!.value.isTakingPicture) {
      return null;
    }
    try {
      await cameraController!.setFlashMode(FlashMode.off);
      cameraController!.stopImageStream();
      picture = await cameraController!.takePicture();
      print("skdjslkdjsl ${picture!.path.toString()}");
      setState(() {});
      Get.off(() => ImageView(
            imageFile: picture!,
          ));
    } on CameraException catch (e) {
      debugPrint('Error occured while taking picture: $e');
      return null;
    }
  }

  detectImagesOnFrames() async {
    if (cameraImage != null) {
      var recognitation = await Tflite.runModelOnFrame(
          bytesList: cameraImage!.planes.map((e) {
            return e.bytes;
          }).toList(),
          imageHeight: cameraImage!.height,
          imageWidth: cameraImage!.width,
          imageMean: 127.5,
          imageStd: 127.5,
          rotation: 90,
          numResults: 2,
          threshold: 0.1,
          asynch: true);
      result = "";
      for (var element in recognitation!) {
        result = element.toString();
        print("Result ${element["label"]}");
        if (element["label"].toString() == "water bottle" ||
            element["label"].toString() == "computer keyboard") {
          print("Capturubg");
          takePicture();
        }
        // XFile picture = await cameraController!.takePicture();
        // picture.saveTo(data);
        // cameraController!.dispose();
        // Get.off(() => Main());
        // cameraController!.takePicture().then((value) {
        //   data = value.toString();
        //
        // });
      }
    }
    setState(() {
      result;
    });
    isWorking = false;
  }

  initCamera() async {
    cameraController = CameraController(cameras[0], ResolutionPreset.high);
    cameraController!.initialize().then((value) {
      if (!mounted) {
        return;
      }
      setState(() {
        cameraController!.startImageStream((image) {
          if (!isWorking) {
            isWorking = true;
            cameraImage = image;
            detectImagesOnFrames();
          }
        });
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initCamera();
    loadModel();
  }

  @override
  void dispose() async {
    // TODO: implement dispose
    super.dispose();
    await Tflite.close();
    cameraController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
              height: double.infinity,
              child: !cameraController!.value.isInitialized
                  ? Container()
                  : result == "manav"
                      ? Container(
                          height: double.infinity,
                          child: Image.file(File(picture!.path.toString())),
                        )
                      : Stack(
                          children: [
                            SizedBox(
                              height: double.infinity,
                              child: AspectRatio(
                                aspectRatio:
                                    cameraController!.value.aspectRatio,
                                child: CameraPreview(cameraController!),
                              ),
                            ),
                            Text(result),
                          ],
                        ))),
    );
  }
}
